<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Data\Enum;
/**
 * Description of Group
 *
 * @author Paweł
 */
class Group {
    const NONE = 0;
    const USER = 1;
    const ADMIN = 2;
}
