<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Core\Base\Controller;

/**
 * Description of PanelController
 *
 * @author Paweł
 */
class PanelController extends Controller {

    const NAME = 'Panel';

    public function indexAction() {
        return $this->render(self::NAME . ':index.html.twig');
    }

}
