<?php

namespace App\Controller;

use Core\Base\Controller;

class GameController extends Controller{
    
    const NAME = 'Game';
    
    public function indexAction(){
        
        
        return $this->render(self::NAME.":index.html.twig");
    }
    
}
