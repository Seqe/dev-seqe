<?php

namespace App\Controller;

use App\Data\Enum\Group;
use App\Database\UsersManager;
use Core\Base\Controller;
use Core\Base\Encoder;
use Core\Base\FormBuilder;
use Core\Validators\Email;
use Core\Validators\Equal;
use Core\Validators\NotBlank;
use Core\Validators\Unique;
use DateTime;

/**
 * Description of MainController
 *
 * @author Paweł
 */
class MainController extends Controller {

    const NAME = 'Main';

    public function indexAction() {
        $user = $this->getSession()->getUser();
        if($user !== null){
            return $this->redirect('/game');
        }
        return $this->redirect('/main/login');
    }

    public function loginAction() {
        $request = $this->getRequest();

        $form = new FormBuilder();

        if ($request->hasPost())
        {
            $data = $request->get('post');
            $form->bindRequest($data)
                    ->addFieldConstraint('email', new NotBlank())
                    ->addFieldConstraint('password', new NotBlank());
            if($form->isValid()){
                $usersManager = $this->getDatabase()->getManager('UsersManager'); /* @var $usersManager UsersManager */
                $user = $usersManager->findUserByEmail($data['email']);
                if($user){
                    $encoder = new Encoder($data['email']);
                    $salt = $encoder->encode($user['joined']);
                    if($user['password'] == md5($data['password'].$salt)){
                        $remeber_me = (isset($data['remember_me']));
                        $groups = $usersManager->getUserGroups($user['id']);
                        $user['groups'] = $groups;
                        unset($user['password']);
                        $this->getSession()->setUser($user);
                        $this->getSession()->setRememberMe($remeber_me);
                        $this->getSession()->addFlash('success', 'Witaj '.$user['username'].'!');
                        return $this->redirect('/game');
                    }
                    else{
                        $this->getSession()->addFlash('danger', 'Podane hasło jest nieprawidłowe.');                        
                    }
                }
                else{
                    $this->getSession()->addFlash('danger', 'Nie odnaleziono podanego adresu email w bazie.');
                }
            }
        }
        return $this->render(self::NAME . ':login.html.twig', array('form'=>$form));
    }

    public function registerAction() {
        $request = $this->getRequest();

        $form = new FormBuilder();

        if ($request->hasPost())
        {
            $data = $request->get('post');
            $form->bindRequest($data)
                    ->addFieldConstraint('email', new NotBlank())
                    ->addFieldConstraint('email', new Unique('users', $this->getDatabase(), false, 'Ten adres email jest już zajęty.'))
                    ->addFieldConstraint('email', new Email())
                    ->addFieldConstraint('password', new NotBlank())
                    ->addFieldConstraint('repassword', new NotBlank())
                    ->addFormConstraint(array('password', 'repassword'), new Equal('Podane hasła nie są takie same.'), 'repassword');
            if ($form->isValid())
            {
                $date = new DateTime('NOW');
                $encoder = new Encoder($data['email']);
                $salt = $encoder->encode($date->format('Y-m-d H:i:s'));
                $newUser = array(
                    'username' => $data['email'],
                    'email' => $data['email'],
                    'password' => md5($data['password'] . $salt),
                    'joined' => $date->format('Y-m-d H:i:s')
                );
                $usersManager = $this->getDatabase()->getManager('UsersManager'); /* @var $usersManager UsersManager */
                $result = $usersManager->persist($newUser, true);
                if ($result['result'])
                {
                    $usersManager->addUserGrop($result['id'], Group::USER);
                    $this->getSession()->addFlash('success', 'Rejestracja została zakończona. Na twój adres email wysłany został link potwierdzający.');
                    $encoder = new Encoder('registerConfirm');
                    $hash = $encoder->encode('register:' . $result['id'] . ':' . $data['email']);
                    $adres = $_SERVER['SERVER_NAME'] . '/main/confirm';
                    $confirmLink = $_SERVER['SERVER_NAME'] . '/main/confirm/' . $hash;
                    $mailer = $this->getMailer();
                    //Konfiguracja mailera dla tej akcji
                    $mailer->setFrom('dev@seqe.pl', 'Dev::Seqe');
                    $mailer->addAddress($data['email']);
                    $mailer->WordWrap = 50;
                    $mailer->isHTML(true);
                    $mailer->Subject = 'Witamy w Dev::Seqe!';
                    $mailer->Body = '<h4>Witaj ' . $data['email'] . '</h4>' .
                            'Otrzymałeś/aś tę wiadomość, ponieważ zarejestrowałeś/aś się w serwisie Dev::Seqe.<br />' .
                            'Aby potwierdzić adres email klinij ten <a href="' . $confirmLink . '">link</a>.<br />' .
                            'Możesz również wkleić podany kod na stronie <a href="' . $adres . '">potwierdzania adresu</a>.<br/>' .
                            '<span style="text-align:center; weight:bold;">' . $hash . '</span>';
                    $mailer->AltBody = 'Skopiuj ten link i otwórz go w swojej przeglądarce: ' . $confirmLink;

                    $mailer->send();

                    return $this->redirect('/');
                } else
                {
                    $this->getSession()->addFlash('danger', 'Wystąpił błąd. Spróbuj ponownie.');
                }
            }
        }

        return $this->render(self::NAME . ':register.html.twig', array('form' => $form));
    }

    public function logoutAction() {
        $cookie = $this->getCookie();
        $cookie->clear();
        $this->getSession()->setUser(null);
        return $this->redirect('/');
    }

    public function printMessagesAction() {
        return $this->render(self::NAME . ':messages.html.twig');
    }

}
