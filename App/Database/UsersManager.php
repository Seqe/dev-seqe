<?php

namespace App\Database;

use Core\Base\Manager;
use Core\Interfaces\ManagerInterface;
use PDO;

/**
 * Klasa zawiera zbiór metod do obsługi bazy danych w kontekście wskazanej tabeli.
 *
 * @author Paweł
 */
class UsersManager extends Manager implements ManagerInterface {

    /**
     * Nazwa tabeli.
     */
    const NAME = 'users';

    /**
     * Sprawdza czy w bazie istnieje użytkownik z podanym loginem i hasłem i zwraca go, lub false jeśli użytkownik nie istnieje.
     * 
     * @param string $username
     * @param string $password
     * @return boolean|array
     */
    public function findUserWithUsernameAndPassword($username, $password) {
        $db = $this->getDataContext();
        $sql = "SELECT * FROM ".self::NAME." WHERE `username`='" . $username . "' AND `password`='" . $password . "' LIMIT 1";
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            if(count($rows) > 0){
                return $rows[0];
            }
            return false;
        }
        return false;
    }
    
    public function findUserByEmail($email) {
        $db = $this->getDataContext();
        $sql = "SELECT * FROM ".self::NAME." WHERE `email`='" . $email . "' LIMIT 1";
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            if(count($rows) > 0){
                return $rows[0];
            }
            return false;
        }
        return false;
    }
    
    public function addUserGrop($id, $group){
        $db = $this->getDataContext();
        $sql = 'INSERT INTO `users_groups` (user_id,group_id) VALUES ('.$id.','.$group.')';
        $query = $db->prepare($sql);
        $result = $query->execute();
        return $result;
    }
    
    public function findById($id) {
        $db = $this->getDataContext();
        $sql = "SELECT * FROM ".self::NAME." WHERE `id`=".$id." LIMIT 1";
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            if(count($rows) > 0){
                return $rows[0];
            }
            return false;
        }
        return false;
    }
    
    /**
     * Pobiera grupy użytkownika.
     * 
     * @param integer $id
     * @return boolean|array
     */
    public function getUserGroups($id){
        $db = $this->getDataContext();
        $sql = "SELECT groups.name as name FROM users_groups LEFT JOIN groups ON users_groups.group_id = groups.id WHERE users_groups.user_id = :user_id";
        $query = $db->prepare($sql);
        $result = $query->execute(array(':user_id'=>$id));
        if($result){
            $rows = $query->fetchAll();
            $groups = array();
            foreach($rows as $row){
                $groups[] = $row['name'];
            }
            return $groups;
        }
        return false;
    }

}
