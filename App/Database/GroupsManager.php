<?php

namespace App\Database;

use Core\Base\Manager;
use Core\Interfaces\ManagerInterface;

/**
 * Klasa zawiera zbiór metod do obsługi bazy danych w kontekście wskazanej tabeli.
 *
 * @author Paweł
 */
class GroupsManager extends Manager implements ManagerInterface {

    /**
     * Nazwa tabeli.
     */
    const NAME = 'groups';    

}
