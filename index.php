<?php

use Core\Base\Controller;
use Core\Base\Cookie;
use Core\Base\Database;
use Core\Base\PageError;
use Core\Base\Request;
use Core\Base\Security;
use Core\Base\Session;
use Core\Update;

include('Core/config.php');
include('Core/security.php');
$request = new Request();
if ($request->getController() == 'error')
{
    $errorController = new PageError($request, null, $twig, null, null, null, null);
    $errorController->displayError($request->getAction());
    die;
}
if ($request->getController() == '__gteup')
{
    $updater = new Update();
    $updater->run();
    die;
}


$controllerClassName = '\\App\\Controller\\' . ucfirst($request->getController()) . 'Controller';

if (class_exists($controllerClassName))
{
    $cookie = new Cookie();
    $session = new Session($cookie);
    $db = new Database($dbhost, $dbname, $dbuser, $dbpass);
    $security = new Security($sec, $session, $request);
    $controllerClass = new $controllerClassName($request, $session, $twig, $db, $security, $mailer, $cookie);

    $methodName = $request->getAction() . 'Action';
    if (method_exists($controllerClass, $methodName))
    {
        if (!$security->hasPermission())
        {
            $session->addFlash('danger', 'Nie masz uprawnień do przeglądania tej strony.');
            $controllerClass->redirect('/');
        }
        $reflectionMethod = new ReflectionMethod($controllerClass, $methodName);
        $requiredParams = $reflectionMethod->getNumberOfRequiredParameters();
        $allParams = $reflectionMethod->getNumberOfParameters();
        $paramsInUrl = count($request->getParams());
        if ($allParams == 0)
        {
            $controllerClass->$methodName();
        } else
        {
            if ($requiredParams > $paramsInUrl)
            {
//                echo "Wskazana akcja potrzebuje podania $requiredParams parametrów.";
                $controllerClass->redirect('/error/404');
            } else
            {
                $params = $request->getParams();
                $paramsArray = array();
                for ($i = 0; $i < $allParams; $i++)
                {
                    if (isset($params[$i]))
                    {
                        $paramsArray[] = $params[$i];
                    }
                }
                call_user_func_array(array($controllerClass, $methodName), $params);
            }
        }
    } else
    {
//        echo "Akcja nie sitnieje
        $controllerClass->redirect('/error/404');
    }
} else
{
//    echo "Kontroler nie istnieje.";
    $controller = new Controller($request, null, $twig, null, null, null, null, null);
    $controller->redirect('/error/404');
}
