<?php

namespace Core\Interfaces;

/**
 * Description of Validator
 *
 * @author Paweł
 */
interface ValidatorInterface{
    public function validate($value);
    public function setName($name);
    public function getName();
}