<?php

namespace Core\Interfaces;

/**
 * @author Paweł
 */
interface ManagerInterface {
    public function persist($data);
}
