<?php

namespace Core\Validators;

use Core\Base\Debug;
use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Unique extends Validator implements ValidatorInterface {

    public $table;
    public $db;
    public $edit;

    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Ta wartość musi być unikalna.';
    public $noConnection = 'Nie można sprawdzić wartości, brak połączenia z bazą danych.';

    public function __construct($table, $db, $edit = false, $message = false) {
        $this->table = $table;
        $this->db = $db;
        $this->edit = $edit;
        if ($message)
        {
            $this->message = $message;
        }
    }

    public function validate($value) {
        $errors = array();
        if ($this->db->getDataContext())
        {
            $db = $this->db->getDataContext();
            if ($this->edit)
            {
                $sql = "SELECT count(*) AS number FROM `" . $this->table . "` WHERE `" . $this->name . "`='" . $value . "' AND `" . $this->name . "`!='" . $this->edit . "'";
            } else
            {
                $sql = "SELECT count(*) AS number FROM `" . $this->table . "` WHERE `" . $this->name . "`='" . $value . "'";
            }
            $query = $db->prepare($sql);
            $result = $query->execute();
            if ($result)
            {
                $rows = $query->fetchAll();
                if ($rows[0]['number'] > 0)
                {
                    $errors[] = $this->message;
                }
            }
        } else
        {
            $errors[] = $this->noConnection;
        }
        return $errors;
    }

}
