<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Email extends Validator implements ValidatorInterface{

    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Podany adres e-mail nie jest prawidłowy.';
    
    public function __construct($message = false) {
        if ($message)
        {
            $this->message = $message;
        }        
    }

    public function validate($value) {
        $errors = array();
        if ($value != '')
        {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL))
            {
                $errors[] = $this->message;
            }
        }
        return $errors;
    }

}
