<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Length extends Validator implements ValidatorInterface{

    /**
     *  Parametry walidatora.
     */
    public $min;
    public $max;

    /**
     *  Komunikaty walidatora.
     */
    public $tooShort = 'Podana wartość jest za krótka.';
    public $tooLong = 'Podana wartość jest za długa.';

    /**
     * Sprawdza czy podana wartość mieści się we wskazanych limitach. Jeśli zamiast limitu podstawiony jest false, będzie on zignorowany.
     * 
     * @param integer|boolean $min
     * @param integer|boolean $max
     */
    public function __construct($min = false, $max = false, $tooShort = false, $tooLong = false) {
        if ($tooShort)
        {
            $this->tooShort = $tooShort;
        }
        if ($tooLong)
        {
            $this->tooLong = $tooLong;
        }
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($value) {
        $errors = array();
        if ($value != '')
        {
            if ($this->min)
            {
                if (strlen($value) < $this->min)
                {
                    $errors[] = $this->tooShort;
                }
            }
            if ($this->max)
            {
                if (strlen($value) > $this->max)
                {
                    $errors[] = $this->tooLong;
                }
            }
        }
        return $errors;
    }

}
