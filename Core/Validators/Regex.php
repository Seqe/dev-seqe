<?php

namespace Core\Validators;

use Core\Base\Debug;
use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Regex extends Validator implements ValidatorInterface{

    protected $regex;
    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Wartość nie jest zgodna z wyrażeniem regularnym';
    
    public function __construct($regex, $message = false) {
        if ($message)
        {
            $this->regex = $regex;
            $this->message = $message;
        }        
    }

    public function validate($value) {
        $errors = array();
        if ($value != '')
        {
            if(!preg_match($this->regex, $value)){
                $errors[] = $this->message;
            }
        }
        return $errors;
    }

}
