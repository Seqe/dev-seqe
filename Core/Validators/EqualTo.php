<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Porównuje wartość pola do podanej wartości.
 *
 * @author Paweł
 */
class EqualTo extends Validator implements ValidatorInterface{

    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Podana wartość jest nieprawidłowa.';
    
    private $compare = '';
    private $modifier = 0;

    public function __construct($compare, $modifier = 0, $message = false) {
        if ($message)
        {
            $this->message = $message;
        }
        $this->compare = $compare;
        $this->modifier = $modifier;
    }

    public function validate($value) {
        $errors = array();
        if($this->modifier == 1){
            $value = md5($value);
        }
        if($value != $this->compare){
            $errors[] = $this->message;
        }
        return $errors;
    }

}
