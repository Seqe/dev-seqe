<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class NotBlank extends Validator implements ValidatorInterface{
    
    /**
     *  Komunikaty walidatora.
     */
    public $message = 'To pole nie może być puste.';
    
    public function __construct($message = false) {
        if($message){
            $this->message = $message;
        }
    }
    
    
    public function validate($value){
        $errors = array();
        if($value == '' || $value == null){
            $errors[] = $this->message;
        }
        return $errors;
    }
}
