<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Equal extends Validator implements ValidatorInterface{

    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Te pola powinny być identyczne.';

    public function __construct($message = false) {
        if ($message)
        {
            $this->message = $message;
        }
    }

    public function validate($values) {
        $errors = array();
        $lastValue = false;
        foreach ($values as $value)
        {
            if ($lastValue)
            {
                if ($value['value'] != $lastValue)
                {
                    $errors[] = $this->message;
                }
            }
            $lastValue = $value['value'];
        }
        return $errors;
    }

}
