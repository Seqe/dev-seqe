<?php

namespace Core\Validators;

use Core\Base\Validator;
use Core\Interfaces\ValidatorInterface;
use DateTime;

/**
 * Description of Length
 *
 * @author Paweł
 */
class Date extends Validator implements ValidatorInterface {

    /**
     *  Komunikaty walidatora.
     */
    public $message = 'Podana wartość nie jest prawidłową datą.';

    public function __construct($message = false) {
        if ($message)
        {
            $this->message = $message;
        }
    }

    public function validate($value) {
        $errors = array();
        if ($value != '')
        {
            $d = DateTime::createFromFormat('Y-m-d', $value);
            if(!($d && $d->format('Y-m-d') == $value)){
                $errors[] = $this->message;
            }
        }
        return $errors;
    }

}
