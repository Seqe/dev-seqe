<?php

/* 
 * Do tablicy pod konkretną grupą użytkownika należy dodać kontroler, 
 * lub konkretną akcję do którego grupa ma mieć uprawnienia, 
 * pierwsze wystąpienie uprawnienia kończy wyszukiwanie.
 */

/**
 * Lista akcji dostępnych dla niezalogowanego użytkownika.
 */
$none = array(
    'main',
    'error'    
);

/**
 * Lista akcji dostępnych dla użytkownika
 */
$user = array(
    'main',
    'error',
    'panel',
    'game'
);

/**
 * Lista akcji dostępnych dla administratora
 */
$admin = array(
    
);

$sec = array(
    'NONE'  => $none,
    'USER'  => $user,
    'ADMIN' => $admin,
);