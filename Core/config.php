<?php

$dev = true;
$dbname = '';
$dbhost = '';
$dbuser = '';
$dbpass = '';

$mailerHost = '';
$mailerSmtpAuth = true;
$mailerUsername = '';
$mailerPassword = '';
$mailerSmtpSecure = 'ssl'; //tls lub ssl
$mailerPort = 465;

if ($dev)
{
    ini_set("display_errors", "on");
    error_reporting(E_ALL | E_STRICT);
}

include('vendors/vendor/autoload.php');

spl_autoload_register(function ($pClassName) {
    $classFile = str_replace("\\", "/", $pClassName) . '.php';
    if (file_exists($classFile))
    {
        include($classFile);
    } else
    {
        return false;
    }
}
);

Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('App/Views');
$twig = new Twig_Environment($loader, array(
    'cache' => ($dev) ? false : 'App/cache',
    'debug' => $dev
        ));
if ($dev)
{
    $twig->addExtension(new Twig_Extension_Debug());
}
$mailer = new PHPMailer();
$mailer->isSMTP();
$mailer->Host = $mailerHost;
$mailer->SMTPAuth = $mailerSmtpAuth;
$mailer->Username = $mailerUsername;
$mailer->Password = $mailerPassword;
$mailer->SMTPSecure = $mailerSmtpSecure;
$mailer->Port = $mailerPort;
