<?php

namespace Core\Base;

/**
 * Description of Controller
 *
 * @author Paweł
 */
class Command {

    private $request;
    private $db;
    private $mailer;

    public function __construct($db, $mailer) {
        $this->db = $db;
        $this->mailer = $mailer;
    }
    
    /**
     * 
     * @return \PHPMailer
     */
    public function getMailer(){
        return $this->mailer;
    }

    /**
     * @return Database
     */
    public function getDatabase() {
        return $this->db;
    }
}
