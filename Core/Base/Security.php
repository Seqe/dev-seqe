<?php

namespace Core\Base;

/**
 * @author Paweł
 */
class Security {

    private $permissions;
    private $session;
    private $request;

    public function __construct($permissions, $session, $request) {
        $this->permissions = $permissions;
        $this->session = $session;
        $this->request = $request;
    }
    
    /**
     * Metoda sprawdza czy użytkownik posiada wskazaną rolę.
     * 
     * @param string $role
     * @return boolean
     */
    public function hasRole($role){
        $user = $this->session->getUser();
        if ($user === null)
        {
            $groups[] = 'NONE';
        } else
        {
            $groups = $user['groups'];
        }
        if(in_array($role, $groups)){
            return true;
        }
        return false;
    }

    /**
     * Metoda sprawdza czy użytkownik ma uprawnienia do przeglądania strony w aktualnym żądaniu,
     * 
     * @return boolean
     */
    public function hasPermission() {
        $hasPermission = false;
        $user = $this->session->getUser();
        $groups = array();
        if ($user === null)
        {
            $groups[] = 'NONE';
        } else
        {
            $groups = $user['groups'];
        }
        if(empty($groups)){
            $groups[] = 'NONE';
        }
        foreach($groups as $group){
            if(isset($this->permissions[$group])){
                if(in_array($this->request->getController(),$this->permissions[$group])){
                    $hasPermission = true;
                    break;
                }
                if(in_array($this->request->getController().'/'.$this->request->getAction(), $this->permissions[$group])){
                    $hasPermission = true;
                    break;
                }
            }
        }
        return $hasPermission;
    }

}
