<?php

namespace Core\Base;

use PDO;

/**
 * Klasa rozszerza managery bazy danych.
 * 
 * @author Paweł
 */
class Manager {

    private $db;

    /**
     * Przypisuje instancję PDO do Manager'a
     * 
     * @param PDO $db
     */
    public function setDataContext($db) {
        $this->db = $db;
    }

    /**
     * Pobiera instację PDO przypisaną do Manager'a
     * 
     * @return PDO
     */
    public function getDataContext() {
        return $this->db;
    }

    /**
     * Wyszukuje w tabeli powiązanej z managerem rekordu o wskazanym ID.
     * 
     * @param integer $id
     * @return array|boolean
     */
    public function findById($id) {
        $db = $this->getDataContext();
        $sql = "SELECT * FROM " . static::NAME . " WHERE `id`=" . $id . " LIMIT 1";
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            if (count($rows) > 0)
            {
                return $rows[0];
            }
            return false;
        }
        return false;
    }

    /**
     * Pobiera z tabeli powiązanej z managerem wszystkich rekordów.
     * 
     * @return array|boolean
     */
    public function findAll() {
        $db = $this->getDataContext();
        $sql = "SELECT * FROM " . static::NAME;
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
        return false;
    }
    
    /**
     * Pobiera z tabeli powiązanej z managerem rekordów, ograniczając zapytanie limitem.
     * 
     * @return array|boolean
     */
    public function findLimit($limit, $start = 0){
        $db = $this->getDataContext();
        $sql = "SELECT * FROM " . static::NAME.' LIMIT '.$start.','.$limit;
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
        return false;
    }
    
    /**
     * Pobiera z tabeli powiązanej z managerem rekordów, ograniczając zapytanie warunkiem oraz limitem.
     * 
     * @param string $where
     * @param integer $limit
     * @param integer $start
     * @param string $direction
     * @return array|boolean
     */
    public function findLimitWhere($where, $limit, $start = 0, $direction = 'asc'){
        $db = $this->getDataContext();
        $sql = "SELECT * FROM " . static::NAME.' WHERE '.$where.' ORDER BY id '.$direction. ' LIMIT '.$start.','.$limit;
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
        return false;
    }
    
    /**
     * Zwraca ilość rekordów w tabeli powiązanej z managerem. 
     * Opcjonalnie można dodać warunek wyszukiwania.
     * 
     * @param string $condition
     * @return int
     */
    public function count($condition = ''){
        $db = $this->getDataContext();
        if($condition != ''){
            $condition = 'WHERE '.$condition;
        }
        $sql = "SELECT count(id) as number FROM ".static::NAME." ".$condition." LIMIT 1";
        $query = $db->prepare($sql);
        $result = $query->execute();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            return $rows[0]['number'];
        }
        return 0; 
    }

    /**
     * Metoda usuwa rekordy z tabeli powiązanej z managerem. Jako parametr 
     * przyjmuje id rekordu do usunięcia, lub tablicę rekordów.
     * 
     * @param integer|array $id
     * @return type
     */
    public function remove($id) {
        $db = $this->getDataContext();
        if (is_array($id))
        {
            $ids = array();
            foreach ($id as $row)
            {
                $ids[] = 'id=' . $row['id'];
            }
            $sql = "DELETE FROM " . static::NAME . " WHERE " . implode(" OR ", $ids);
            $query = $db->prepare($sql);
            $result = $query->execute();
            return $result;
        }
        $sql = "DELETE FROM " . static::NAME . " WHERE id=" . $id;
        $query = $db->prepare($sql);
        $result = $query->execute();
        return $result;
    }

    /**
     * Dodaje rekord do bazy. Podanie w drugim parametrze true zwróci zamiast 
     * wartości boolean z powodzeniem, lub niepowodzeniem zapytania, tablicę
     * z tą informacją, oraz id dodanego rekordu. 
     * 
     * @param array $data
     * @param boolean $insertId
     * @return boolean|array
     */
    public function persist($data, $insertId = true) {
        $db = $this->db;
        $preparedData = array();
        $fields = array();
        $keys = array();
        foreach ($data as $key => $value)
        {
            $fields[] = $key;
            $keys[] = ":" . $key;
            $preparedData[":" . $key] = $value;
        }
        $sql = 'INSERT INTO ' . static::NAME . ' (' . implode(',', $fields) . ') VALUES (' . implode(',', $keys) . ')';
        $db->beginTransaction();
        $query = $this->db->prepare($sql);
        $result = $query->execute($preparedData);
        $id = $db->lastInsertId();
        $db->commit();
        if ($insertId)
        {
            return array(
                'result' => $result,
                'id' => $id
            );
        }
        return $result;
    }

    /**
     * Metoda aktualizuje rekord w bazie. Do poprawnego działania tablica podana 
     * w parametrze musi posiadać pole id
     * 
     * @param array $data
     * @return boolean
     */
    public function update($data) {
        $preparedData = array();
        $id = 0;
        $sets = array();
        foreach ($data as $key => $value)
        {
            if (!is_array($value))
            {
                if ($key == 'id')
                {
                    $id = $value;
                } else
                {
                    $sets[] = $key . "=:" . $key . "";
                }
                $preparedData[":" . $key] = $value;
            }
        }
        $sql = 'UPDATE ' . static::NAME . ' SET ' . implode(',', $sets) . ' WHERE id=:id';
        $query = $this->db->prepare($sql);
        $result = $query->execute($preparedData);
        return $result;
    }
    
    /**
     * Tworzy instancję QueryBuilder'a dla obecnej tabeli
     * 
     * @return QueryBuilder
     */
    public function getQueryBuilder(){
        $queryBuilder = new QueryBuilder(static::NAME);
        return $queryBuilder;
    }

}
