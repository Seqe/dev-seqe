<?php

namespace Core\Base;

use PHPMailer;
use ReflectionMethod;

/**
 * Description of Controller
 *
 * @author Paweł
 */
class Controller {

    private $request;
    private $session;
    private $twig;
    private $db;
    private $security;
    private $mailer;
    private $cookie;

    public function __construct($request, $session, $twig, $db, $security, $mailer, $cookie) {
        $this->request = $request;
        $this->session = $session;
        $this->twig = $twig;
        $this->db = $db;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->cookie = $cookie;
    }

    /**
     * 
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }
    
    /**
     * 
     * @return PHPMailer
     */
    public function getMailer(){
        return $this->mailer;
    }

    /**
     * Zwraca klasę sesji.
     * 
     * @return Session
     */
    public function getSession() {
        return $this->session;
    }
    
    /**
     * Zwraca obiekt Cookie.
     * 
     * @return Cookie
     */
    public function getCookie(){
        return $this->cookie;
    }

    /**
     * Renderowanie akcji. Do używania w Twig'u. Jeśli mamy akcję która tworzy fragment strony np. menu to tą metodą będziemy mogli ją dodać.
     * 
     * @param string $url Url do akcji którą chcemy utworzyć.
     * @return View
     */
    public function renderAction($url) {
        $request = new Request();
        $request->parseUri($url);
        $controllerClassName = '\\App\\Controller\\' . ucfirst($request->getController()) . 'Controller';

        if (class_exists($controllerClassName))
        {
            $controllerClass = new $controllerClassName($this->request, $this->session, $this->twig, $this->db, $this->security, $this->mailer, $this->cookie);

            $methodName = $request->getAction() . 'Action';
            if (method_exists($controllerClass, $methodName))
            {
                if (!$this->security->hasPermission())
                {
                    $this->session->addFlash('danger', 'Nie masz uprawnień do przeglądania tej strony.');
                    $controllerClass->redirect('/');
                }
                $reflectionMethod = new ReflectionMethod($controllerClass, $methodName);
                $requiredParams = $reflectionMethod->getNumberOfRequiredParameters();
                $allParams = $reflectionMethod->getNumberOfParameters();
                $paramsInUrl = count($request->getParams());
                if ($allParams == 0)
                {
                    $controllerClass->$methodName();
                } else
                {
                    if ($requiredParams > $paramsInUrl)
                    {
                        $controllerClass->redirect('/error/404');
                    } else
                    {
                        $params = $request->getParams();
                        $paramsArray = array();
                        for ($i = 0; $i < $allParams; $i++)
                        {
                            if (isset($params[$i]))
                            {
                                $paramsArray[] = $params[$i];
                            }
                        }
                        return call_user_func_array(array($controllerClass, $methodName), $params);
                    }
                }
            } else
            {
                $controllerClass->redirect('/error/404');
            }
        } else
        {
            $this->redirect('/error/404');
        }
    }
    

    /**
     * Metoda renderuje wskazaną templatkę z podanymi parametrami
     * 
     * @param string $template Nazwa templatki do wyrenderowania
     * @param array $data Tablica parametrów do wstrzyknięcia w templatkę.
     */
    public function render($template, $data = array()) {
        $pathToTemplate = '/' . str_replace(':', '/', $template);

        $app = array(
            'session' => $this->session,
            'controller' => $this,
            'request' => $this->request
        );
        $data['app'] = $app;

        $template = $this->twig->loadTemplate($pathToTemplate);
        echo $template->render($data);
    }

    /**
     * Zwraca dane w postaci JSON. Do wykrozystywania w przypadku np. żądań ajaxowych.
     * 
     * @param type $data
     */
    public function jsonResponse($data) {
        if (is_array($data))
        {
            echo json_encode($data);
        } else
        {
            echo $data;
        }
    }

    /**
     * Generuje przekierowanie na inną stronę.
     * 
     * @param string $url 
     */
    public function redirect($url) {
        header('Location: http://' . $_SERVER['SERVER_NAME'] . $url);
        die;
    }

    /**
     * @return Database
     */
    public function getDatabase() {
        return $this->db;
    }

    /**
     * @return Security
     */
    public function getSecurity() {
        return $this->security;
    }

}
