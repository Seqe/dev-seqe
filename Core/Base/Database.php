<?php

namespace Core\Base;

use Core\Interfaces\ManagerInterface;
use PDO;
use PDOException;

/**
 * Klasa służy do przechowywania instacji PDO.
 *
 * @author Paweł
 */
class Database {

    private $dataContext;

    public function __construct($host, $dbname, $user, $pass, $type = 'mysql') {
        try {
            $dbh = new PDO($type . ':host=' . $host . ';dbname=' . $dbname, $user, $pass);
            $this->dataContext = $dbh;
        } catch (PDOException $ex) {
            $this->dataContext = false;
        }
    }

    /**
     * Zwraca instację PDO przechowywaną w tym obiekcie, 
     * lub w przypadku gdy instancja nie została właściwie zainicjowana, 
     * zwraca false
     * 
     * @return PDO|boolean
     */
    public function getDataContext() {
        return $this->dataContext;
    }

    /**
     * Tworzy i zwraca obiekt manager'a
     * 
     * @param string $manager Nazwa managera do utworzenia
     * @return ManagerInterface
     */
    public function getManager($manager) {
        if ($this->dataContext)
        {
            $managerClassName = '\\App\\Database\\' . $manager;
            if (class_exists($managerClassName))
            {
                $manager = $this->loadManager(new $managerClassName());
                $manager->setDataContext($this->dataContext);
                return $manager;
            }
        } else
        {
            echo "Nie można nazwiązać połączenia z bazą danych. Sprawdź konfigurację.";
            die;
        }
    }
    
    /**
     * Metoda służy do sprawdzania czy zaciągnięty manager jest zgodny z interfejsem
     * @param ManagerInterface $manager
     */
    private function loadManager(ManagerInterface $manager){
        return $manager;
    }
}
