<?php

namespace Core\Base;

/**
 * Description of PageError
 *
 * @author Paweł
 */
class PageError extends Controller {
    
    /**
     * Wyświetla stronę z błędem
     * 
     * @param string $error Numer błędu
     * @return View
     */
    public function displayError($error){
        $content = '';
        switch($error){
            case '404': $content = $error; break;
            default: $content = '404'; break;
        }  
        return $this->render('ErrorPage:'.$content.'.html.twig');
    }
}
