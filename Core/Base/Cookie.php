<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Base;

/**
 * Description of Cookie
 *
 * @author Paweł
 */
class Cookie {

    private $data;

    public function __construct() {
        $this->data = $_COOKIE;
    }

    public function get($key = '') {
        if ($key == '')
        {
            return $this->data;
        }
        if (array_key_exists($key, $this->data))
        {
            return $this->data[$key];
        }
        return false;
    }

    public function set($key, $value, $expire = 2629743) {
        setcookie($key, $value, time() + $expire);
        $this->data = $_COOKIE;
    }
    
    public function remove($key){
        setcookie($key, '', time() - 1000);
        $this->data = $_COOKIE;
    }

    public function clear() {
        if (isset($_SERVER['HTTP_COOKIE']))
        {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie)
            {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
        $this->data = $_COOKIE;
    }

}
