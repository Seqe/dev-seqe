<?php

namespace Core\Base;

/**
 * Własna klasa kodująca dane z możliwością ich późniejszego dekodowania.
 *
 * @author Paweł
 */
class Encoder {
    
    private $key;

    /**
     * Tworzy instancję Encoder'a
     * 
     * @param string $key Klucz wg. którego mają być kodowane, lub dekodowane dane.
     */
    public function Encoder($key) {
        $this->key = $key;
    }
    
    /**
     * Tworzenie hasha na podstawie klucza.
     * 
     * @param string $string
     * @return string
     */
    public function encode($string) {        
        $this->key = sha1($this->key);
        $strLen = strlen($string);
        $keyLen = strlen($this->key);
        $j = null;
        $hash = '';
        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($string, $i, 1));
            if ($j == $keyLen) {
                $j = 0;
            }
            $ordKey = ord(substr($this->key, $j, 1));
            $j++;
            $hash .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
        }
        return $hash;
    }

    /**
     * Dekoduje hash Encodera na podstawie klucza
     * 
     * @param string $hash
     * @return string
     */
    public function decode($hash) {
        $this->key = sha1($this->key);
        $strLen = strlen($hash);
        $keyLen = strlen($this->key);
        $j = null;
        $string = '';
        for ($i = 0; $i < $strLen; $i+=2) {
            $ordStr = hexdec(base_convert(strrev(substr($hash, $i, 2)), 36, 16));
            if ($j == $keyLen) {
                $j = 0;
            }
            $ordKey = ord(substr($this->key, $j, 1));
            $j++;
            $string .= chr($ordStr - $ordKey);
        }
        return $string;
    }

}