<?php

namespace Core\Base;

/**
 * Klasa z metodami statycznymi do debugowania
 *
 * @author Paweł
 */
class Debug {
    
    static public function dump($array, $depth = 1, $die=true, $indentation = 0) {
        if (is_array($array))
        {
            echo "Array(\n";
            foreach ($array as $key => $value)
            {
                if (is_array($value))
                {
                    if ($depth)
                    {
//                        echo "max depth reached.";
                    } else
                    {
                        for ($i = 0; $i < $indentation; $i++)
                        {
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                        echo $key . "=Array(";
                        $this->dump($value, $depth - 1, $indentation + 1);
                        for ($i = 0; $i < $indentation; $i++)
                        {
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                        echo ");";
                    }
                } else
                {
                    for ($i = 0; $i < $indentation; $i++)
                    {
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    echo $key . "=>" . $value . "\n";
                }
            }
            echo ");\n";
        } else
        {
            var_dump($array);
        }
        if($die) die;
    }

}
