<?php

namespace Core\Base;

use Core\Interfaces\ValidatorInterface;

/**
 * Generowanie formularzy.
 *
 * @author Paweł
 */
class FormBuilder {

    protected $hasErrors = false;
    protected $fields = array();
    protected $formErrors = array();
    protected $selectsOptions = array();

    /**
     * Metoda dodaje pole do formularza oraz błędy jeśli zostały znalezione.
     * 
     * @param string $name Nazwa pola formularza.
     */
    public function addField($name) {
        $this->fields[$name] = array(
            'value' => '',
            'errors' => array()
        );
    }

    /**
     * Aby poprawnie wskazywać aktualnie wybrane pole Select np. podczas edycji,
     * należy je dodawać w postaci tablicy dla FormBuilder'a
     * 
     * @param string $name Nazwa pola
     * @param array $array Tablica danych pola wyboru
     * @return \Core\Base\FormBuilder
     */
    public function addSelect($name, $array) {
        $this->selectsOptions[$name] = $array;
        return $this;
    }

    /**
     * Metoda dla Twig'a, która generuje pole wyboru oznaczając aktualnie 
     * wybrane pole tagiem 'selected'
     * 
     * @param string $name Nazwa pola do wyrenderowania.
     * @return string Kod HTML z polem wyboru.
     */
    public function renderSelect($name) {
        $select = '<select class="form-control" name="' . $name . '">';
        foreach ($this->selectsOptions[$name] as $key => $value)
        {
            $selected = "";
            if ($this->getValue($name) == $key)
            {
                $selected = "selected";
            }
            $select .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    /**
     * Metoda zwraca aktualnie przypisaną wartość do pola
     * 
     * @param string $name Nazwa pola dla którego zwrócone mają być dane.
     * @return string
     */
    public function getValue($name) {
        if (!isset($this->fields[$name]))
        {
            return '';
        }
        return $this->fields[$name]['value'];
    }

    /**
     * Pobiera błędy, które zostały dodane do wskazanego pola.
     * 
     * @param string $name Nazw pola dla którego zwrócone mają być dane.
     * @return array|boolean
     */
    public function getErrors($name) {
        if (!isset($this->fields[$name]))
        {
            return false;
        }
        if (empty($this->fields[$name]['errors']))
        {
            return false;
        }
        return $this->fields[$name]['errors'];
    }

    /**
     * Wiąże tablicę danych z żądania z formularzem.
     * 
     * @param array $data
     * @return \Core\Base\FormBuilder
     */
    public function bindRequest($data) {
        foreach ($data as $key => $value)
        {
            $this->fields[$key] = array(
                'value' => $value,
                'errors' => array()
            );
        }
        return $this;
    }

    /**
     * Wiąże tablicę danych (np. z bazy danych) z formularzem.
     * 
     * @param array $data
     * @return \Core\Base\FormBuilder
     */
    public function bindData($data) {
        foreach ($data as $key => $value)
        {
            if (!is_array($value))
            {
                $this->fields[$key] = array(
                    'value' => $value,
                    'errors' => array()
                );
            }
        }
        return $this;
    }

    /**
     * Dodaje do pola walidator.
     * 
     * @param string $field Nazwa pola do którego ma być przypisany walidator.
     * @param \Core\Interfaces\ValidatorInterface $constraint Obiekt walidatora.
     * @return \Core\Base\FormBuilder
     */
    public function addFieldConstraint($field, ValidatorInterface $constraint) {
        $constraint->setName($field);
        $errors = $constraint->validate($this->fields[$field]['value']);
        foreach ($errors as $error)
        {
            $this->hasErrors = true;
            $this->fields[$field]['errors'][] = $error;
        }
        return $this;
    }

    /**
     * Dodaje do formularza walidator.
     * 
     * @param array $fields Pola które mają zostać zwalidowane. Kolejność ma znaczenie!
     * @param \Core\Interfaces\ValidatorInterface $constraint Obiekt walidatora formularza.
     * @param string|boolean $placeError Opcjonalny parametr wskzujący do którego pola 
     * przypisany ma być błąd, brak oznacza przypisanie błędu do formularza.
     * @return \Core\Base\FormBuilder
     */
    public function addFormConstraint($fields, ValidatorInterface $constraint, $placeError = false) {
        $constraint->setName($fields);
        $validateFields = array();
        foreach ($fields as $field)
        {
            $validateFields[$field] = $this->fields[$field];
        }
        $errors = $constraint->validate($validateFields);
        foreach ($errors as $error)
        {
            $this->hasErrors = true;
            if ($placeError)
            {
                $this->fields[$placeError]['errors'][] = $error;
            } else
            {
                $this->formErrors[] = $error;
            }
        }
        return $this;
    }

    /**
     * Metoda dla Twig'a renderująca błędy formularza. Podanie w drugim parametrze 
     * true zwróci klasę css has-error w przypadku gdy pole zawiera błędy.
     * 
     * @param string $field
     * @param boolean $class
     * @return string
     */
    public function renderErrors($field, $class = false) {
        $errors = $this->getErrors($field);
        if ($class)
        {
            if ($errors)
            {
                return 'has-error';
            }
            return '';
        }
        if ($errors)
        {
            $printError = '<span class="help-block"><ul>';
            foreach ($errors as $error)
            {
                $printError .= '<li>' . $error . '</li>';
            }
            $printError .= '</ul></span>';
            return $printError;
        }
        return '';
    }

    /**
     * Metoda sprawdza czy formularz zawiera w sobie błędy.
     * 
     * @return boolean
     */
    public function isValid() {
        return !$this->hasErrors;
    }

}
