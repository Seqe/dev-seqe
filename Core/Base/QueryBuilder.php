<?php

namespace Core\Base;

/**
 * Description of QueryBuilder
 *
 * @author Paweł
 */
class QueryBuilder {

    private $sql = '';
    private $table = '';
    private $sqlType = 'select';
    private $columns = array();
    private $wheres = array();
    private $orWheres = array();
    private $joins = array();
    private $limit = 0;
    private $start = 0;
    private $orders = array();
    private $orderType = 'asc';
    private $transMap = array();

    public function __construct($table) {
        $this->table = $table;
    }

    /**
     * Ustawia typ generowanego zapytania na Select.
     * 
     * @return \Core\Base\QueryBuilder
     */
    public function select() {
        $this->sqlType = 'select';
        return $this;
    }

    /**
     * Dodaje do zapytania listę kolumn.
     * 
     * @param array $array
     * @return \Core\Base\QueryBuilder
     */
    public function columns($array) {
        foreach ($array as $elem)
        {
            if (strpos($elem,'.') === false)
            {
                $this->columns[] = $this->table . '.' . $elem;
            } else
            {
                $this->columns[] = $elem;
            }
        }
        return $this;
    }

    /**
     * Dodaje do zapytania kolumnę.
     * 
     * @param string $column
     * @return \Core\Base\QueryBuilder
     */
    public function addColumn($column) {
        if (strpos($column,'.') === false)
        {
            $this->columns[] = $this->table . '.' . $column;
        } else
        {
            $this->columns[] = $column;
        }
        return $this;
    }

    /**
     * Dodaje do zapytania warunek, który będzie łączony z innymi za pomocą AND.
     * 
     * @param string $where
     * @return \Core\Base\QueryBuilder
     */
    public function addWhere($where) {
        $this->wheres[] = '(' . $where . ')';
        return $this;
    }

    /**
     * Dodaje do zapytania warunek, który będzie łączony z innymi za pomocą OR.
     * 
     * @param string $where
     * @return \Core\Base\QueryBuilder
     */
    public function addOrWhere($where) {
        $this->orWheres[] = '(' . $where . ')';
        return $this;
    }

    /**
     * Dodaje do zapytania klauzulę łączenia z inną tabelą
     * 
     * @param string $join
     * @return \Core\Base\QueryBuilder
     */
    public function join($join) {
        $this->joins[] = $join;
        return $this;
    }

    /**
     * Dodaje do zapytania translację nazw kolumn. 
     * Należy podawać je w formie 'nazwa.w_bazie'=>'translacja'
     * 
     * @param array $array
     * @return \Core\Base\QueryBuilder
     */
    public function transMap($array) {
        $this->transMap = $array;
        return $this;
    }

    /**
     * Ustawia limit zapytania. Jeśli limit ustawiony jest na 0 zapytanie nie będzie ograniczane.
     * 
     * @param integer $limit
     * @param integer $start
     * @return \Core\Base\QueryBuilder
     */
    public function setLimit($limit, $start = 0) {
        $this->limit = $limit;
        $this->start = $start;
        return $this;
    }

    /**
     * Dodaje do zapytania klauzulę sortowania. Kolejność ustalana wg. dodania do klasy.
     * 
     * @param array|string $order
     * @param string $type
     * @return \Core\Base\QueryBuilder
     */
    public function orderBy($order, $type = 'asc') {
        $this->orderType = $type;
        if (is_array($order))
        {
            foreach ($order as $elem)
            {
                $this->orders[] = $elem;
            }
            return $this;
        }
        $this->orders[] = $order;
        return $this;
    }

    /**
     * Zwraca wygenerowane zapytanie sql.
     * 
     * @return string
     */
    public function getSql() {
        switch ($this->sqlType) {
            case 'select': $this->generateSelect();
                break;
        }
        return $this->sql;
    }

    private function generateSelect() {
        $columnsString = '';
        if (empty($this->columns))
        {
            $columnsString = '*';
        } else
        {
            $columns = array();
            foreach ($this->columns as $column)
            {
                if (isset($this->transMap[$column]))
                {
                    $columns[] = $column . ' AS ' . $this->transMap[$column];
                } else
                {
                    $columns[] = $column;
                }
            }
            $columnsString = implode(', ', $columns);
        }
        $joinsString = '';
        if (!empty($this->joins))
        {
            $joinsString = implode(' ', $this->joins);
        }
        $wheresString = '';
        if (!empty($this->wheres))
        {
            $wheresString = implode(' AND ', $this->wheres);
        }
        if (!empty($this->orWheres))
        {
            $orWheresString = implode(' OR ', $this->orWheres);
            if ($wheresString === '')
            {
                $wheresString = $orWheresString;
            } else
            {
                $wheresString .= ' OR ' . $orWheresString;
            }
        }
        if($wheresString !== ''){
            $wheresString = 'WHERE '.$wheresString;
        }
        $ordersString = '';
        if (!empty($this->orders))
        {
            $ordersString = 'ORDER BY ' . implode(', ', $this->orders) . ' ' . $this->orderType;
        }

        $limitsString = '';
        if ($this->limit != 0)
        {
            $limitsString = 'LIMIT ' . $this->start . ', ' . $this->limit;
        }

        $this->sql = 'SELECT ' . $columnsString . ' FROM ' . $this->table . ' ' . $joinsString . ' ' . $wheresString . ' ' . $ordersString . ' ' . $limitsString;
        return $this->sql;
    }

}
