<?php

namespace Core\Base;

/**
 * Obsługa sesji.
 *
 * @author Paweł
 */
class Session {

    private $data;
    private $flashBag = array();
    private $user;
    private $cookie;

    public function __construct(Cookie $cookie) {
        $this->cookie = $cookie;
        session_start();
        $this->data = $_SESSION;
        if (isset($_SESSION['flashes'])) {
            $this->flashBag = $_SESSION['flashes'];
        }
        if (isset($_SESSION['user'])) {
            $this->user = $_SESSION['user'];
        } else {
            $this->user = null;
        }
        if ($this->cookie->get('token') && $this->user == null) {
            $encoder = new Encoder('token::gtengine');
            $user = json_decode($encoder->decode($this->cookie->get('token')), true);
            $this->user = $user;
        }
    }

    public function setRememberMe($val) {
        $token = $this->cookie->get('token');
        if ($token) {
            if ($val) {
                $this->cookie->set('token', $token, time() + 31536000);
                $this->cookie->set('remeber_me', true, time() + 31536000);
            } else {
                $this->cookie->set('token', $token, time() + 1800);
                $this->cookie->remove('remember_me');
            }
        }
    }

    /**
     * Dodaje wiadomość flash do sesji.
     * 
     * @param string $type
     * @param string $message
     */
    public function addFlash($type, $message) {
        if (!isset($this->flashBag[$type])) {
            $this->flashBag[$type] = array();
        }
        $this->flashBag[$type][] = $message;
        $_SESSION['flashes'] = $this->flashBag;
    }

    /**
     * Pobiera tablicę wszystkich wiaodmości flashowych z bazy, 
     * lub  wskazanych w parametrze.
     * 
     * @param string|boolean $type
     * @return array
     */
    public function getFlashBag($type = false) {
        if ($type) {
            if (isset($this->flashBag[$type])) {
                $flashes = $this->flashBag[$type];
                $this->flashBag[$type] = array();
                $_SESSION['flashes'] = $this->flashBag;
                return $flashes;
            }
            return array();
        }
        $flashes = $this->flashBag;
        $this->flashBag = array();
        $_SESSION['flashes'] = $this->flashBag;
        return $flashes;
    }

    /**
     * Pobiera zalogowanego użytkownika z sesji.
     * 
     * @return array
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Przypisuje użytkownika do sesji.
     * 
     * @param array $user
     * @return Session
     */
    public function setUser($user) {
        $this->user = $user;
        $_SESSION['user'] = $this->user;
        $encoder = new Encoder('token::gtengine');
        $token = $encoder->encode(json_encode($this->user));
        if ($this->cookie->get('remember_me')) {
            $this->cookie->set('token', $token, time() + 31536000);
        } else {
            $this->cookie->set('token', $token, time() + 1800);
        }
        return $this;
    }

    /**
     * Metoda sprawdza czy użytkownik posiada wskazaną rolę.
     * 
     * @param string $role
     * @return boolean
     */
    public function hasRole($role) {
        $user = $this->user;
        if ($user === null) {
            $groups[] = 'NONE';
        } else {
            $groups = $user['groups'];
        }
        if (in_array($role, $groups)) {
            return true;
        }
        return false;
    }

}
