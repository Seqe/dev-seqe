<?php

namespace Core\Base;

use PDO;

/**
 * Klasa służy do przetwarzania żądań server-side plugin'u Datatables. Generuje zapytanie do bazy, pobiera dane i zwraca w pożądanej przez plugin formie
 *
 * @author Paweł
 */
class DataTable {

    private $columns;
    private $db;
    private $table;
    private $data;
    private $sql;
    private $where;
    private $join = array();

    /**
     * @param PDO $db Instancja PDO.
     * @param array $data Dane przesyłane na serwer metodą POST, lub GET przez plugin datatables.
     * @param string $table Nazwa tabeli do której odnosi się żądanie.
     */
    public function __construct($db, $data, $table) {
        $this->table = $table;
        $this->db = $db->getDataContext();
        $this->data = $data;
        $this->columns = array();
    }
    
    /**
     * Metoda umożliwia dodanie do zapytania własnych restrykcji np. wyświetlanie danych tylko dla wskazanego użytkownika.
     * 
     * @param string $statement
     */
    public function where($statement){
        $this->where = $statement;
    }
    
    /**
     * Metoda służy do łączenia tabel w zapytaniu do bazy danych.
     * 
     * @param string $table Nazwa dołączanej tabeli.
     * @param string $on Warunek dołączenia
     * @param string $type Typ łączenia. Domyślny LEFT
     * @return \Core\Base\DataTable
     */
    public function join($table, $on, $type = 'LEFT'){
        $this->join[] = array(
            'table' => $table,
            'on' => $on,
            'type' => $type
        );
        return $this;
    }

    /**
     * Generuje tablicę danych dotyczącą kolumny dla DataTables
     * 
     * @param string $dbName Nazwa kolumny po stronie bazy danych
     * @param integer $dtIdentifier Identyfikator kolumny w pluginie datatables
     * @param function|boolean $formatter Opcjonalny parametr przyjmuje funckję anonimową do formatowania wskazanej kolumny po stronie serwera.
     * @return DataTable
     */
    public function addColumn($dbName, $dtIdentifier, $formatter = false) {
        $column = array(
            'db' => $dbName,
            'dt' => $dtIdentifier
        );
        if ($formatter)
        {
            $column['formatter'] = $formatter;
        }
        $this->columns[] = $column;
        return $this;
    }

    /**
     * Zwraca nazwy wszystkich kolumn dodanych w tej instancji klasy.
     * 
     * @return array
     */
    private function getDbColumns() {
        $array = array();
        foreach ($this->columns as $column)
        {
            $array[] = $column['db'];
        }
        return $array;
    }

    /**
     * Generuje wyrażenie WHERE do zapytania
     * 
     * @return string
     */
    private function generateFilter() {
        $whereStatement = "";
        $cols = $this->getDbColumns();
        $colNumber = count($cols);
        if($this->where != ""){
            $whereStatement = "WHERE ".$this->where;
        }
        //Globalne wyszukiwanie w tabeli
        if (isset($this->data['search']) && $this->data['search'] != "")
        {
            if($whereStatement == ""){
                $whereStatement = "WHERE (";
            }
            else{
                $whereStatement .= " AND (";
            }
            for ($i = 0; $i < $colNumber; $i++)
            {
                if (isset($this->data['columns'][$i]['searchable']) && $this->data['columns'][$i]['searchable'] == "true")
                {
                    $whereStatement .= $cols[$i] . " LIKE '%" . $this->data['search']["value"] . "%' OR ";
                }
            }
            $whereStatement = substr_replace($whereStatement, "", -3);
            $whereStatement .= ")";
        }
        //Wyszukiwanie w indywidualnych kolumnach
        for ($i = 0; $i < $colNumber; $i++)
        {
            if (isset($this->data['columns'][$i]['searchable']) && $this->data['columns'][$i]['searchable'] == "true" && $this->data['columns'][$i]['search']['value'] != "")
            {
                if ($whereStatement == "")
                {
                    $whereStatement = "WHERE ";
                } else
                {
                    $whereStatement .= " AND ";
                }
                $whereStatement .= $cols[$i] . " LIKE '%" . $this->data['columns'][$i]['search']['value'] . "%' ";
            }
        }
        return $whereStatement;
    }

    /**
     * Generuje wyrażenie ORDER do zapytania
     * 
     * @return string
     */
    private function generateSort() {
        $sortStatement = "";
        $cols = $this->getDbColumns();
        if (isset($this->data['order']))
        {
            $sortStatement = "ORDER BY  ";
            $sortCols = count($this->data['order']);
            for ($i = 0; $i < $sortCols; $i++)
            {
                $sortStatement .= $cols[intval($this->data['order'][$i]['column'])] . "
                    " . ($this->data['order'][$i]['dir'] === 'asc' ? 'asc' : 'desc') . ", ";
            }

            $sortStatement = substr_replace($sortStatement, "", -2);
            if ($sortStatement == "ORDER BY")
            {
                $sortStatement = "";
            }
        }
        return $sortStatement;
    }

    /**
     * Generuje wyrażenie LIMIT do zapytania
     * 
     * @return string
     */
    private function generatePaging() {
        $pagingStatement = "";
        if (isset($this->data['length']) && $this->data['length'] != '-1')
        {
            $pagingStatement = "LIMIT " . intval($this->data['start']) . ", " .
                    intval($this->data['length']);
        }
        return $pagingStatement;
    }
    
    /**
     * Generuje wyrażenie łączenia tabel.
     * 
     * @return string
     */
    private function generateJoin(){
        $joinStatement = array();
        if(empty($this->join)){
            return "";
        }
        
        foreach($this->join as $join){
            $joinStatement[] = $join['type']." JOIN ".$join['table']." ON ".$join['on'];
        }
        
        return implode(" ",$joinStatement);        
    }

    /**
     * Generuje kompletne zapytanie SQL
     * 
     * @return string
     */
    private function createSql() {
        $sql = "SELECT SQL_CALC_FOUND_ROWS " .
                implode(', ', $this->getDbColumns()) .
                " FROM " . $this->table . " " .
                $this->generateJoin() . " " .
                $this->generateFilter() . " " .
                $this->generateSort() . " " .
                $this->generatePaging();
        return $sql;
    }

    /**
     * Oblicza i zwraca ilość wszystkich rekordów odpytywanej tabeli uwzględniając indywidualnie dodany warunek.
     * 
     * @return integer
     */
    private function getTotal() {
        $sql = "SELECT COUNT(id) as 'number' FROM " . $this->table;
        if($this->where != ''){
            $sql .= " WHERE ".$this->where;
        }
        $query = $this->db->prepare($sql);
        $result = $query->execute();
        $total = 0;
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            $total = $rows[0]['number'];
        }
        return $total;
    }

    /**
     * Oblicza i zwraca ilość wszystkich rekordów, które spełniają warunki nadane indywidualnie dla żądania, oraz te przesłane przez plugin datatables.
     * 
     * @return integer
     */
    private function getTotalMatching() {
        $query = $this->db->prepare("SELECT FOUND_ROWS() as total");
        $result = $query->execute();
        $total = 0;
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            $total = $rows[0]['total'];
        }
        return $total;
    }

    /**
     * Zwraca tablcę zgodną z standardem odbieranych danych przez plugin datatables.
     * 
     * @return array
     */
    public function process() {
        $query = $this->db->prepare($this->createSql());
        $result = $query->execute();
        $cols = $this->getDbColumns();
        $colsNumber = count($cols);
        if (!isset($this->data['sEcho']))
        {
            $this->data['sEcho'] = '';
        }
        $totalMatch = $this->getTotalMatching();
        $output = array(
            'sEcho' => intval($this->data['sEcho']),
            'iTotalRecords' => intval($this->getTotal()),
            'iTotalDisplayRecords' => $totalMatch
        );
        $data = array();
        if ($result)
        {
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows as $row)
            {
                $sRow = array();
                for ($i = 0; $i < $colsNumber; $i++)
                {
                    $columnName = $cols[$i];
                    if(strpos($columnName,'.') !== false){
                        $temp = explode(".",$columnName);
                        $columnName = end($temp);
                    }
                    if (isset($this->columns[$i]['formatter']))
                    {
                        $formatted = $this->columns[$i]['formatter']($row[$columnName], $row);
                        $sRow[] = $formatted;
                    } else
                    {
                        $sRow[] = $row[$columnName];
                    }
                }
                $data[] = $sRow;
            }
            $output['aaData'] = $data;
        }
        return $output;
    }

}
