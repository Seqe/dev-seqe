<?php

namespace Core\Base;

/**
 * Generator menu
 *
 * @author Paweł
 */
class MenuCreator {

    private $currentUri;
    private $currentController;
    private $security;
    private $menu;

    public function __construct($security, $request) {
        $this->security = $security;
        $this->menu = array();
        $this->currentUri = $request->getUri();
        $this->currentController = $request->getController(false);
    }

    /**
     * Dodaje kontener do menu.
     * 
     * @param string $containerName Identyfikator kontenera.
     * @param string $title Wyświetlana nazwa kontenera.
     * @param string|boolean $action Opcjonalnie można przypisać url do kontenera.
     * @param string $icon Nazwa ikony do podczepienia w kontenerze.
     * @return \Core\Base\MenuCreator
     */
    public function addMenuContainer($containerName, $title, $action = false, $icon = '') {
        $active = false;
        if ($action)
        {
            $active = ($this->currentUri == $action);
        }
        $this->menu[$containerName] = array(
            'type' => 'cont',
            'name' => $title,
            'icon' => $icon,
            'action' => $action,
            'active' => $active,
            'pages' => array()
        );
        return $this;
    }

    /**
     * Dodaje akcję do kontenera.
     * 
     * @param string $containerName Nazwa kontenera do którego dodana będzie akcja.
     * @param string $title Wyświetlana nazwa akcji.
     * @param string $route Url akcji.
     * @return \Core\Base\MenuCreator
     */
    public function addMenuAction($containerName, $title, $route) {
        $active = ($this->currentUri == $route);
        if ($containerName)
        {
            $this->menu[$containerName]['pages'][] = array(
                'type' => 'page',
                'action' => $route,
                'active' => $active,
                'name' => $title
            );
            if ($active)
            {
                $this->menu[$containerName]['active'] = true;
            }
        }

        return $this;
    }

    /**
     * Zwraca tablicę z danymi menu.
     * 
     * @return array
     */
    public function getMenu() {
        return $this->menu;
    }

}
