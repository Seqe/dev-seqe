<?php

namespace Core\Base;

/**
 * Przetwarzanie żądań
 *
 * @author Paweł
 */
class Request {

    private $controller = '';
    private $action = '';
    private $params = array();

    /**
     * Konstruktor pobiera i przetwarza aktualny URL.
     */
    public function __construct() {
        $this->parseUri($_SERVER['REQUEST_URI']);
    }

    /**
     * Zwraca aktualny URL
     * 
     * @return string
     */
    public function getUri() {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Przetwarza podany URL
     * 
     * @param string $uri
     */
    public function parseUri($uri) {
        if ($uri == '/')
        {
            $this->controller = 'main';
            $this->action = 'index';
        } else
        {
            if (strpos($uri, '?') !== false)
            {
                $parse = explode('?', $uri);
                $uri = $parse[0];
                if (array_key_exists(1, $parse))
                {
                    $data = explode('&', $parse[1]);
                    foreach ($data as $param)
                    {
                        if ($param != "")
                        {
                            $getKeyValue = explode('=', $param);
                            $_GET[$getKeyValue[0]] = $getKeyValue[1];
                        }
                    }
                }
            }
            $urlData = explode("/", $uri);
            $paramNr = 0;
            foreach ($urlData as $node)
            {
                switch ($paramNr) {
                    case 0:break;
                    case 1: $this->controller = $node;
                        break;
                    case 2: $this->action = $node;
                        break;
                    default: $this->params[] = $node;
                }
                $paramNr++;
            }
            if ($this->controller == '')
            {
                $this->controller = 'main';
            }
            if ($this->action == '')
            {
                $this->action = 'index';
            }
        }
    }

    /**
     * Zamienia element URL-a do postaci camel np.
     * element URL: nazwa-ackji-dla-url
     * camel: nazwaAkcjiDlaUrl
     * 
     * @param string $name
     * @return string
     */
    private function convertToCamel($name) {
        if (strpos($name, '-') !== false)
        {
            $nodes = explode('-', $name);
            $camel = '';
            $pos = 0;
            foreach ($nodes as $node)
            {
                if ($pos != 0)
                {
                    $camel .= ucfirst($node);
                } else
                {
                    $camel .= $node;
                }
                $pos++;
            }
            return $camel;
        }
        return $name;
    }

    /**
     * Pobiera nazwę kontrolera z żądania
     * 
     * @param boolean $camel
     * @return string
     */
    public function getController($camel = true) {
        if ($camel)
        {
            return $this->convertToCamel($this->controller);
        }
        return $this->controller;
    }

    /**
     * Pobiera nazwę akcji z żądania
     * 
     * @param boolean $camel
     * @return string
     */
    public function getAction($camel = true) {
        if ($camel)
        {
            return $this->convertToCamel($this->action);
        }
        return $this->action;
    }

    /**
     * Zwraca parametry z żądania.
     * 
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Sprawdza czy w żadaniu zostały wysłane dane metodą POST.
     * 
     * @return boolean
     */
    public function hasPost() {
        if (!empty($_POST))
        {
            return true;
        }
        return false;
    }

    /**
     * Pobieranie tablicy danych z żadania metody POST i GET
     * 
     * @param string $type
     * @return array
     */
    public function get($type) {
        $value = array();
        switch ($type) {
            case 'post': $value = $_POST;
                break;
            case 'get': $value = $_GET;
                break;
        }
        return $value;
    }

    /**
     * Sprawdza czy żądanie pochodzi z ajaxa.
     * 
     * @return boolean
     */
    public function isAjax() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        }
        return false;
    }

}
