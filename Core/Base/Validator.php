<?php
namespace Core\Base;

/**
 * Klasa rozszerzajaca walidatory
 *
 * @author Paweł
 */
class Validator {
    
    protected $name;
    
    /**
     * Pobiera nazwę walidowanego pola.
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Przypisuje nazwę walidowanego pola
     * 
     * @param string $name
     * @return \Core\Base\Validator
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }


}
